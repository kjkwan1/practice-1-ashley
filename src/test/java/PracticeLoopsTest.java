import org.junit.*;
import static org.junit.Assert.*;

public class PracticeLoopsTest {

    PracticeLoops p = new PracticeLoops();

    @Test
    public void printEven20(){
        String even20 = "0 2 4 6 8 10 12 14 16 18 20 ";
        String even10 = "0 2 4 6 8 10 ";

        Assert.assertEquals(even20, p.printEven20(20));
        Assert.assertEquals(even10, p.printEven20(10));
        Assert.assertEquals("0 2 4 6 8 10 12 14 16 18 20 22 24 26 ", p.printEven20(26));

    }

    @Test
    public void sumEven20(){
        Assert.assertEquals(36, p.sumEven20(11));
        Assert.assertEquals(100, p.sumEven20(20));
        Assert.assertEquals(64, p.sumEven20(15));
    }

    @Test
    public void oddArray(){
        int[] test1 = {2, 3, 9, 10, 73, 53, 22, 91, 13, 48};
        int[] test2 = {13, 6, 18, 27, 193, 100, 55, 32, 73, 88};
        int[] test3 = {2, 8, 10, 13, 7, 9, 73, 27, 18, 99};

        Assert.assertEquals("3 9 73 53 91 13 ", p.oddArray(test1));
        Assert.assertEquals("13 27 193 55 73 ", p.oddArray(test2));
        Assert.assertEquals("13 7 9 73 27 99 ", p.oddArray(test3));
    }

    @Test
    public void printEvenSum(){
        int[] test1 = {2, 3, 9, 10, 73, 53, 22, 91, 13, 48};
        int[] test2 = {13, 6, 18, 27, 193, 100, 55, 32, 73, 88};
        int[] test3 = {2, 8, 10, 13, 7, 9, 73, 27, 18, 99};

        Assert.assertEquals(82, p.printEvenSum(test1));
        Assert.assertEquals(244, p.printEvenSum(test2));
        Assert.assertEquals(38, p.printEvenSum(test3));
    }

    @Test
    public void startsY(){
        Assert.assertEquals(false, p.startsY("hello"));
        Assert.assertEquals(true, p.startsY("yello"));
        Assert.assertEquals(true, p.startsY("ydime"));
        Assert.assertEquals(true, p.startsY("ysl"));
        Assert.assertEquals(true, p.startsY("yomkippur"));
        Assert.assertEquals(false, p.startsY("dom"));

    }

    @Test
    public void startsX(){
        String[] test1 = {"xhi", "why", "bye", "xlye", "sky", "xfly", "xdie", "xsly", "dye", "pie"};
        String[] test2 = {"hi", "xwhy", "xbye", "lye", "skyx", "xflxy", "dixe", "slyx", "xdye", "pxie"};

        Assert.assertEquals("xhi xlye xfly xdie xsly ", p.startsX(test1));
        Assert.assertEquals("xwhy xbye xflxy xdye ", p.startsX(test2));

    }

    @Test
    public void removeZero(){

        Assert.assertEquals("zero", p.removeZero("z0e0r00o"));
        Assert.assertEquals("IDidIt", p.removeZero("0ID0id0I0t"));
        Assert.assertEquals("CodingGod", p.removeZero("C0odi0n0gG0od"));
    }

    @Test
    public void removeY(){

        String[] test1 =  {"dime", "ylime", "slime", "ycrime", "yprime", "mine", "divine", "whine", "yalign", "pine"};
        String[] test2 = {"yhi", "why", "ybye", "ylye", "sky", "fly", "ydie", "sly", "dye", "yxie"};

        Assert.assertEquals("lime crime prime align ", p.removeY(test1));
        Assert.assertEquals("hi be le die xie ", p.removeY(test2));
    }

    @Test
    public void removeOo(){
        String test1 = "woowoowwwoowoooww";
        String test2 = "ooooo";
        String test3 = "wooweeoo";
        String test4 = "oweooooweeeo";

        Assert.assertEquals(5, p.removeOo(test1));
        Assert.assertEquals(4, p.removeOo(test2));
        Assert.assertEquals(2, p.removeOo(test3));
        Assert.assertEquals(3, p.removeOo(test4));

    }

}
